package com.egrower;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChooseGroupAdapter extends RecyclerView.Adapter<ChooseGroupAdapter.MyViewHolder> {

    private List<ChooseGroupModel> listingModelList;
    private Context context;

    public ChooseGroupAdapter(Context context, List<ChooseGroupModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public CircleImageView groupPicture;
        public TextView groupName, groupStatus;
        public Button joinButton;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView)view.findViewById(R.id.card_view);
            groupPicture = (CircleImageView)view.findViewById(R.id.card_group_picture);
            groupName = (TextView) view.findViewById(R.id.card_group_name_text);
            groupStatus = (TextView) view.findViewById(R.id.card_group_status_text);
            joinButton = (Button) view.findViewById(R.id.card_group_join_button);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_choose_group, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ChooseGroupModel documentModel = listingModelList.get(position);

        holder.groupName.setText(documentModel.getName());
        holder.groupStatus.setText("Status : "+documentModel.getStatus());

        holder.joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"join on development", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
