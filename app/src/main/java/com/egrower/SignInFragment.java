package com.egrower;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SignInFragment extends Fragment implements View.OnClickListener {

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        TextView toRegisterText = (TextView)view.findViewById(R.id.sign_in_register_text);
        Button loginButton = (Button)view.findViewById(R.id.sign_in_login_button);

        toRegisterText.setOnClickListener(this);
        loginButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sign_in_register_text :
                ((AuthActivity)getActivity()).setFragment(1);
                break;
            case R.id.sign_in_login_button :
                Toast.makeText(getActivity(),"login on development", Toast.LENGTH_SHORT).show();
                break;
            default :
                break;
        }
    }
}
