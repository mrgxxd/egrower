package com.egrower;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class ChooseGroupFragment extends Fragment {

    // GLOBAL VARIABLE FOR RECYCLE VIEW
    private List<ChooseGroupModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ChooseGroupAdapter mAdapter;

    public static ChooseGroupFragment newInstance() {
        ChooseGroupFragment fragment = new ChooseGroupFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_group, container, false);

        // RecycleView
        recyclerView = (RecyclerView)view.findViewById(R.id.choose_group_recycler_view);
        listingModelList = new ArrayList<>();
        mAdapter = new ChooseGroupAdapter(getActivity(),listingModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        setHardCodeData();

        return view;
    }

    private void setHardCodeData(){
        ChooseGroupModel chooseGroupModel1 = new ChooseGroupModel("1","Petani Metal", "Kosong", "petanimetal.jpg","-", "-", "-", "-");
        listingModelList.add(chooseGroupModel1);

        ChooseGroupModel chooseGroupModel2 = new ChooseGroupModel("2","Petani Pop", "Isi", "petanipop.jpg","1", "Bagus Setiadi", "089111111111", "bagus.jpg");
        listingModelList.add(chooseGroupModel2);

        ChooseGroupModel chooseGroupModel3 = new ChooseGroupModel("3","Petani Jazz", "Penuh", "petanijazz.jpg","2", "Gilar Purwita", "089222222222", "gilar.jpg");
        listingModelList.add(chooseGroupModel3);
    }
}
