package com.egrower;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        int fragmentId = getIntent().getIntExtra("FRAGMENT_ID", 0);

        setFragment(fragmentId);
    }

    public void setFragment(int fragmentId){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragmentId){
            case 0 :
                transaction.replace(R.id.auth_fragment_layout, SignInFragment.newInstance());
                transaction.commit();
                break;
            case 1 :
                transaction.replace(R.id.auth_fragment_layout, SignUpFragment.newInstance());
                transaction.commit();
                break;
            default :
                break;
        }
    }
}
