package com.egrower;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.library.NavigationBar;

public class StepRegisterActivity extends AppCompatActivity {

    private NavigationBar navigationBar;
    private int position = 0;

    private TextView stepTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_register);

        stepTitle = (TextView)findViewById(R.id.step_register_title);

        int fragmentId = getIntent().getIntExtra("FRAGMENT_ID", 0);
        setFragment(fragmentId);
    }

    public void setFragment(int fragmentId){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragmentId){
            case 0 :
                stepTitle.setText("Verifikasi Nomor");
                transaction.replace(R.id.register_fragment_layout, VerificationFragment.newInstance());
                transaction.commit();
                break;
            case 1 :
                stepTitle.setText("Data Diri");
                transaction.replace(R.id.register_fragment_layout, ProfileFragment.newInstance());
                transaction.commit();
                break;
            case 2 :
                stepTitle.setText("Memilih Kelompok");
                transaction.replace(R.id.register_fragment_layout, ChooseGroupFragment.newInstance());
                transaction.commit();
                break;
            default :
                break;
        }
    }
}
