package com.egrower;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        TextView toLoginText = (TextView)view.findViewById(R.id.sign_up_login_text);
        Button registerButton = (Button)view.findViewById(R.id.sign_up_register_button);

        toLoginText.setOnClickListener(this);
        registerButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sign_up_login_text :
                ((AuthActivity)getActivity()).setFragment(0);
                break;
            case R.id.sign_up_register_button :
                //Toast.makeText(getActivity(),"register on development", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), StepRegisterActivity.class);
                startActivity(intent);
                break;
            default :
                break;
        }
    }
}
