package com.egrower;

public class ChooseGroupModel {

    private String id, name, status, image, leaderId, leaderName, leaderContact, leaderImage;

    public ChooseGroupModel(String _id, String _name, String _status, String _image, String _leaderId, String _leaderName, String _leaderContact, String _leaderImage){
        this.id = _id;
        this.name = _name;
        this.status = _status;
        this.image = _image;
        this.leaderId = _leaderId;
        this.leaderName = _leaderName;
        this.leaderContact = _leaderContact;
        this.leaderImage = _leaderImage;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public String getLeaderContact() {
        return leaderContact;
    }

    public String getLeaderImage() {
        return leaderImage;
    }
}
